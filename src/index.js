module.exports = {
    parser: '@typescript-eslint/parser',
    parserOptions: {
        ecmaVersion: 11,
        sourceType: 'module',
        ecmaFeatures: {
            jsx: true
        }
    },
    plugins: [
        'import',
        'jsx-a11y',
        'react-hooks',
        '@typescript-eslint/eslint-plugin'
    // "jest",
    ],
    extends: [
        'eslint:recommended',
        'plugin:react/recommended'
    // "plugin:jest/recommended",
    // "react-app",
    // "react-app/jest",
    ],
    env: {
        browser: true,
        es6: true,
        node: true
    // jest: true,
    },
    rules: {
        quotes: [ 'error', 'single' ],
        // 缩进
        indent: [
            'error',
            4,
            {
                SwitchCase: 1,
                VariableDeclarator: 1,
                ArrayExpression: 1,
                ObjectExpression: 1,
                ImportDeclaration: 1,
                ignoredNodes: [ 'ConditionalExpression' ]
            }
        ],
        // 单行最大长度
        'max-len': [
            'warn',
            {
                code: 120,
                ignoreComments: true,
                ignoreUrls: true,
                ignoreRegExpLiterals: true,
                ignoreStrings: true,
                ignoreTemplateLiterals: true
            }
        ],
        // 最大嵌套深度(4)
        'max-depth': 'error',
        // 不允许尾随逗号
        'comma-dangle': [ 'error', 'never' ],
        // 数组空格
        'array-bracket-spacing': [ 'error', 'always' ],
        // 对象空格
        'object-curly-spacing': [ 'error', 'always' ],
        // 使用全等判断
        eqeqeq: [ 'error', 'always' ],
        'computed-property-spacing': [ 'error', 'never' ],
        // 箭头函数间距样式
        'arrow-spacing': 'error',
        // 在箭头函数体之前不允许换行
        'implicit-arrow-linebreak': 'error',
        // 块内部间隔
        'block-spacing': 'error',
        // 大括号风格
        'brace-style': [ 'error', '1tbs', { allowSingleLine: true } ],
        camelcase: 'off',
        // 逗号空格
        'comma-spacing': [ 'error', { before: false, after: true } ],
        // 逗号样式
        'comma-style': [ 'error', 'last' ],
        // 对象字面量属性中强制在冒号周围放置空格
        'key-spacing': [ 'error', { beforeColon: false, afterColon: true } ],
        // 关键字前后加空格
        'keyword-spacing': 'error',
        // 函数调用不加空格
        'func-call-spacing': [ 'error', 'never' ],
        // 属性前无空格
        'no-whitespace-before-property': 'error',
        // semi分号
        semi: 'error',
        // 格式化函数
        'space-before-function-paren': [
            'error',
            {
                anonymous: 'always',
                named: 'never',
                asyncArrow: 'always'
            }
        ],
        // 扩展运算符
        'rest-spread-spacing': 'error',
        // 注释位置
        'line-comment-position': [ 'error', { position: 'above' } ],
        // 不允许与代码同一行
        'no-inline-comments': 'error',
        // 注释符号前后加空格
        'spaced-comment': [ 'error', 'always', { exceptions: [ '*' ] } ],
        'no-unused-vars': 'warn',
        'no-constant-condition': 'off',
        'space-infix-ops': 'error',
        'space-before-blocks': 'error',
        'react/display-name': 'off',
        'react/prop-types': 'off',
        // 等号周围无空格
        'react/jsx-equals-spacing': 'error',
        // JSX属性双引
        'jsx-quotes': [ 'error', 'prefer-single' ],
        // 属性/表达式括号空格
        'react/jsx-curly-spacing': [ 'error', { when: 'never', children: true } ],
        'react/jsx-closing-bracket-location': [ 'error', 'line-aligned' ],
        'react/jsx-tag-spacing': [
            'error',
            {
                closingSlash: 'never',
                beforeSelfClosing: 'always',
                afterOpening: 'never',
                beforeClosing: 'never'
            }
        ],
        'react-hooks/exhaustive-deps': 'off'
    }
};
